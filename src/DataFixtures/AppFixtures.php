<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(ObjectManager $manager)
    {
        //Utilisation de Faker
        $faker = Factory::create('fr_FR');


        $user = new User();

        $user->setEmail('user@test.fr')
            ->setPrenom($faker->firstName())
            ->setNom($faker->lastName())
            ->setTelephone($faker->phoneNumber());

        $user->setEmail('user@test.com')
            ->setPassword($this->passwordHasher->hashPassword($user, 'password'));

        $manager->persist($user);

        for ($i = 0; $i < 8; $i++) {
            $article = new Article();

            $article->setTitre($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month, now'))
                ->setContenu($faker->text(350))
                ->setSlug(($faker->slug(3)))
                ->setUser($user);

            $manager->persist($article);
        }


        $manager->flush();
    }
}
