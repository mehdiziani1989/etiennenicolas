import utils from './utils';
import navbarInit from './bootstrap-navbar';
import detectorInit from './detector';

const docReady = utils.docReady

// /* -------------------------------------------------------------------------- */
// /*                            Theme Initialization                            */
// /* -------------------------------------------------------------------------- */

docReady(navbarInit);
docReady(detectorInit);

