<?php
namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();
        $user->setEmail('true@test.fr')
      ->setPrenom('prenom')
      ->setNom('nom')
      ->setPassword('password')
      ->setTelephone('0123456789');

        $this->assertTrue($user->getEmail() === 'true@test.fr');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getTelephone() === '0123456789');
    }
    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.fr')
      ->setPrenom('prenom')
      ->setNom('nom')
      ->setPassword('password')
      ->setTelephone('0123456789');

        $this->assertFalse($user->getEmail() === 'false@test.fr');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getTelephone() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getTelephone());
    }
}
