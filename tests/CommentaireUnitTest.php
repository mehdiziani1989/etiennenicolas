<?php

namespace App\Tests;

use DateTime;
use App\Entity\Article;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $article = new Article();

        $commentaire->setAuteur('auteur')
            ->setEmail('email@test.fr')
            ->setContenu('contenu')
            ->setCreatedAt($datetime)
            ->setArticle($article);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.fr');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getArticle() === $article);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $article = new Article();

        $commentaire->setAuteur('auteur')
            ->setEmail('false@test.fr')
            ->setContenu('false')
            ->setCreatedAt(new DateTime())
            ->setArticle(new Article());

        $this->assertFalse($commentaire->getAuteur() === "false");
        $this->assertFalse($commentaire->getEmail() === "false");
        $this->assertFalse($commentaire->getContenu() === "contenu");
        $this->assertFalse($commentaire->getCreatedAt() === $datetime);
        $this->assertFalse($commentaire->getArticle() === $article);
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getArticle());
    }
}
