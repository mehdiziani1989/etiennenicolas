<?php

namespace App\Tests;

use DateTime;
use App\Entity\Article;
use PHPUnit\Framework\TestCase;

class ArticleUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $article = new Article();
        $datetime = new DateTime();

        $article->setTitre('titre')
      ->setCreatedAt($datetime)
      ->setContenu('contenu')
      ->setSlug('slug');

        $this->assertTrue($article->getTitre() === "titre");
        $this->assertTrue($article->getCreatedat() === $datetime);
        $this->assertTrue($article->getContenu() === "contenu");
        $this->assertTrue(($article->getSlug() === "slug"));
    }

    public function testIsFalse()
    {
        $article = new Article();
        $datetime = new DateTime();

        $article->setTitre('titre')
      ->setCreatedAt($datetime)
      ->setContenu('contenu')
      ->setSlug('slug');

        $this->assertFalse($article->getTitre() === "false");
        $this->assertFalse($article->getCreatedat() === new Datetime());
        $this->assertFalse($article->getContenu() === "false");
        $this->assertFalse(($article->getSlug() === "false"));
    }

    public function testIsEmpty()
    {
        $article = new Article();

        $this->assertEmpty($article->getTitre());
        $this->assertEmpty($article->getCreatedat());
        $this->assertEmpty($article->getContenu());
        $this->assertEmpty($article->getSlug());
    }
}
