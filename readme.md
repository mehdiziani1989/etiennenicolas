# EtienneNicolas

Etienne Nicolas est un site vitrine pour un avocat et qui permet de prendre des rendez vous.

### Pré-Requis

- PHP 7.4
- Composer
- Symfony CLI
- MAMP
- node js et yarn

Vérification des pré-requis avec :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
yarn install
yarn run build
symfony serve -d
```

### Ajouter des données de tests

```bash
symfony console doctrine:fixtures:load
```

### Lancer des tests

```bash
 php bin/phpunit --testdox
```
